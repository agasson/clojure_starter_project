(ns crossref.starter-test
  (:require [crossref.starter :refer [synergy-add]]
            [clojure.test :refer [deftest testing is]]))

(deftest ^:unit adding-with-synergy
  (testing "the whole is greater than the sum of elements"
    (is (> (synergy-add) 0))
    (is (> (synergy-add 1 1) 2))
    (is (> (synergy-add 1 2 3) 6))))

