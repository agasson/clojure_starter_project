FROM clojure

COPY . /usr/src/app
WORKDIR /usr/src/app
RUN apt-get update && apt-get install -y git curl unzip
RUN lein deps
RUN curl -o install-clj-kondo https://raw.githubusercontent.com/borkdude/clj-kondo/master/script/install-clj-kondo
RUN bash install-clj-kondo
